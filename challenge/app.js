require('dotenv').config();
const express = require('express');
const router = require('./routes/index.route');
const app = express();
const port = process.env.PORT;

app.use(express.json());
app.use(express.urlencoded( { extended : true } ));

app.use('/', router);


app.listen(port, () => {
    console.log(`saya mendengarkan di port ${port}`);
});

// Penjelasan
/*
Langkah 1 : 
jalankan sequelize db:create 
untuk membuat database

Langkah 2:
jalankan sequelize db:migrate 
untuk menjalankan migration dan membuat table

langkah 3: 
jalankan sequelize db:seed:all
untuk menjalankan seeder dan membuat data yang akan dimasukkan table

Langkah 4:
jalankan npm start

Langkah 5:
akses http://localhost:3000/usergame dengan method GET untuk menampilkan data usergame 

Langkah 6:
akses http://localhost:3000/usergame dengan method POST untuk menambahkan data usergame
dan masukkan datanya yaitu username,password,email 


Langkah 7:
akses http://localhost:3000/usergame/:id dengan method PUT untuk mengupdate data usergame 
semisal idnya yaitu 3 maka akan seperti ini http://localhost:3000/usergame/3
sehinnga yang akan diupdate adalah data dengan id 3 dan masukkan field yang akan di update
yaitu username,password,email

Langkah 8:
akses http://localhost:3000/usergame/:id dengan method DELETE untuk menghapus data usergame 
semisal idnya yaitu 3 maka akan seperti ini http://localhost:3000/usergame/3
sehinnga yang akan dihapus adalah data dengan id 3
*/