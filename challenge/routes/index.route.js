const express = require('express');
const router = express.Router();
const gameRoutes = require('./usergames.route');
const { aunthenticate } = require('../misc/middleware');

router.use('/usergame', aunthenticate, gameRoutes);


module.exports = router;