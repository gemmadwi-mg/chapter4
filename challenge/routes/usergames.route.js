const express = require('express');
const router = express.Router();
const controller = require('../controllers/index')


router.get('/', controller.usergame.getAll);
router.post('/', controller.usergame.post);
router.put('/:user_game_id', controller.usergame.put);
router.delete('/:user_game_id', controller.usergame.delete);

module.exports = router;