const { UserGame,UserGameHistory,UserGameBiodata } = require('../models');
const controller = {};

controller.getAll = async function (req, res) {
    try {
        const options = {
            attributes: ['user_game_id', 'username', 'password', 'email'],
            include: [ 
                {
                    model: UserGameHistory,
                    attributes: ['user_game_history_id', 'skor', 'user_game_id']
                },
                {
                    model: UserGameBiodata,
                    attributes: ['user_game_biodata_id', 'usia', 'user_game_id']
                }
            ]
        };

        const allUserGames = await UserGame.findAll(options);

        if (allUserGames.length > 0) {
            res.status(200).json({
                message: 'succes',
                data: allUserGames
            })
        } else {
            res.status(200).json({
                message: 'tidak ada Data',
                data: allUserGames
            })
        }
    } catch (error) {
        res.status(404).json({
            message: error.message
        })
    }
}

controller.post = async function (req, res) {
    try {
        const { username, password, email } = req.body;

        let createdGame = await UserGame.create({
            username: username,
            password: password,
            email: email
        })
        res.status(201).json({
            message: 'Sukses menambah Data User Game',
            data: createdGame
        })
    } catch (error) {
        console.log(error);
        res.status(404).json({
            message: error.message
        })
    }
}

controller.put = async function (req, res) {
    try {
        await UserGame.update({
            username: req.body.username,
            password: req.body.password,
            email: req.body.email
        }, {
            where: {
                user_game_id: req.params.user_game_id,
            }
        })
        res.status(201).json({
            message: 'Sukses Ubah Data User Game'
        })
    } catch (error) {
        res.status(404).json({
            message: error.message
        })
    }
}

controller.delete = async function (req, res) {
    try {
        await UserGame.destroy({
            where: {
                user_game_id: req.params.user_game_id
            }
        })
        res.status(201).json({
            message: 'Sukses Hapus Data User Game'
        })
    } catch (error) {
        res.status(404).json({
            message: error.message
        })
    }
}

module.exports = controller;